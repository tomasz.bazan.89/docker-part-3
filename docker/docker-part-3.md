## Docker part 3 - Dockerfile

Dockerfile jest to zwykły plik tekstowy, który zawiera wszystkie polecenia potrzebne do zbudowania obrazu.

---
```
https://docs.docker.com/engine/reference/builder/
```

FROM - jest to pierwsza instrukcja w każdym Dockerfile. Informuje na podstawie jakiego obrazu chcemy zbudować nasz obraz (hub.docker.com).

RUN - uruchamia dowolne polecenie na obrazie i tworzy nową warstwę.

ENTRYPOINT - "punkt wejścia" do obrazu, którty zostanie uruchominy przy starcie. Nie może być nadpisany przez parametr polecenia docker run.

CMD - domyśla komenda, która zostanie uruchomiona przy starcie. Te polecenie może być nadpisane.

COPY - kopiuje plik z hosta do obrazu

---

### Ćwiczenie 1

```
https://github.com/docker/whalesay.git
docker build .
docker images ls
docker build . -t whale:latest
```
---
Zmodyfikuj plik Dockerfile tak by po wykonaniu polecenia:
```
docker run mojwhale:first
```
w chmurce pojawił się napis "Hello world", a po wykonaniu:
```
docker run mojwhale:first Tomasz
```
był napis "Hello Tomasz".

---

### Ćwiczenie 2 - Instalacja postgresa

1. Wjedź na 
```
hub.docker.com
```
znajdź obraz postgreSQL uruchom zgodnie z instrukcją jeden kontener o:
- nazwie: mydatabase
- zmiannna środowiskowa: POSTGRES_PASSWORD=mysecretpassword
- uruchomionego w tle
- w wersji: 9.6-alpine

2. Sprawdź czy baza działa tym samym obrazem. Uruchamiając polecenie 
```
psql -h $(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mydatabase) -U postgres
```

---
### Ćwiczenie 3 - Java app

1. Pobranie źródeł z:
```
https://gitlab.com/tomasz.bazan.89/docker-part-3.git
mvn install
```
2. Dodaj plik Dockerfile, który zbuduje obraz:
- na podstawie openjdk:8-jre-slim
- udostępni port 8080 (EXSPOSE)
- doda zmienną środowiskową SPRING_PROFILES_ACTIVE=prod (ENV)
- stworzy katalog /app
- skopuje zbudowaną aplikację z target/java-app-0.0.1-SNAPSHOT.jar do /app jako app.jar
- uruchomi na starcie polecenie java -jar /app/app.jar

---
3. Zbuduj obraz o nazwie java-app
3. Stwórz sieć o nazwie: app
4. Zatrzymaj i usuń kontener mydatabase, a następnie uruchom go jeszcze raz w sieci app (porty nie powinny być wystawione na zewnątrz sieci)
5. Uruchom obraz java-app:
- o nazwie app
- który działa w tle
- wystawia port 8080 konetenera na port 80 hosta
- działa w sieci app

---
test:
```
curl --data "test" localhost/user
curl localhost/user
```

---
### Ćwiczenie 4 - multistage build
1. Zatrzymaj i usuń kontener app
2. Usuń folder target z folderu java-app
3. Zmień plik Dockerfile tak by sam zbudował aplikację przy tworzniu obrazu:
- Obraz do budowania będzie na podstawie maven:3.6.3-jdk-8-openj9 nazwany jako build (FROM XXX AS build)
- skopiuj pom.xml oraz src do /tmp
- poinformuj, że wszsytkie kolejne polecenia będą wykonywana w katalogu /tmp (WORKDIR)
- uruchom polecenie:
```
mvn install
```
4. Aplikacja powina być skopiowana nie z hosta tylko z kroku build.
- zmodyfikuj instrukcję COPY by kopiowała pliki z kroku build (COPY --from=build)
5. Zbuduj i uruchom kontener, a następnie sprawdź czy działa.

---
### Ćwiczenie 5 - Docker-compose
1. Zatrzymaj i usuń wszystkie działające kontenery.
```
docker container stop $(docker container ps -a -q) && docker container rm $(docker container ps -a -q)
```
2. Zamień plik docker-compose.yaml sonara, który po wykonaniu polecenia
```
docker-compose up
```
uruchomi bazę oraz aplikację zbudowaną na podstawie Dockerfile.
3. Sprawdź czy działa
4. Extra: Usunięcie zmiennej środowiskowej z Dockerfile i dodanie jej w docker-compose 

---
#### Pytania?

---
### Rozwiązanie ćwieczenia 1

```
FROM alpine
RUN apk add --no-cache perl
COPY cowsay /usr/local/bin/cowsay
COPY docker.cow /usr/local/share/cows/default.cow
ENTRYPOINT ["/usr/local/bin/cowsay", "Hello"]
CMD ["World"]
```
---
### Rozwiązanie ćwiczenia 2

```
docker run --name mydatabase -e POSTGRES_PASSWORD=mysecretpassword -d postgres:9.6-alpine
docker run -it --rm postgres:9.6-alpine psql -h $(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mydatabase) -U postgres
```
---
### Rozwiązanie ćwiczenia 3

```
docker network create app
docker run --name mydatabase -d --network app -e POSTGRES_PASSWORD=mysecretpassword postgres:9.6-alpine
docker build . -t java-app
docker run --name app -d -p 80:8080 --network app java-app
```
---
docker run -it -p 8080:8080 -v $(pwd)/docker:/app/slides msoedov/hacker-slides
