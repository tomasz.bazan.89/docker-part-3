import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from './user';
import {environment} from '../environments/environment';

@Injectable()
export class JavaService {

  constructor(private http: HttpClient) {

  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('http://localhost:8080/user');
  }

  createUser(user: string): Observable<string> {
    return this.http.post<string>(environment.backendUrl + '/user', user);
  }
}
