import { Component, OnInit } from '@angular/core';
import {JavaService} from '../java.service';
import {User} from '../user';

@Component({
  selector: 'app-java',
  templateUrl: './java.component.html',
  styleUrls: ['./java.component.css']
})
export class JavaComponent implements OnInit {
  users: User[];

  constructor(public javaService: JavaService) {
  }

  ngOnInit(): void {
    this.javaService.getUsers().subscribe(data => {
      this.users = data;
    });
  }

}
