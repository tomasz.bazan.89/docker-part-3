import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {JavaComponent} from './java/java.component';
import {JavaService} from './java.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    JavaComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule
  ],
  providers: [JavaService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
