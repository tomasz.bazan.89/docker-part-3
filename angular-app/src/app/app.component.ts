import {Component, OnInit} from '@angular/core';
import {User} from './user';
import {JavaService} from './java.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  users: User[];

  constructor(public javaService: JavaService) {
    console.log('test');
  }

  ngOnInit(): void {
    this.javaService.getUsers().subscribe(data => {
      this.users = data;
    });
  }

}

