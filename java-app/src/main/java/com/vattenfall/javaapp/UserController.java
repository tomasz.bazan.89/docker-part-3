package com.vattenfall.javaapp;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "Access-Control-Allow-Origin")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/user")
    public void saveUser(@RequestBody String userName) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(userName);
        this.userRepository.save(userEntity);
    }

    @GetMapping("/user")
    public List<UserEntity> getUsers() {
        return this.userRepository.findAll();
    }
}
